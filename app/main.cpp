#include <RectTableGen.h>

#include <iostream>

#include <QCoreApplication>
#include <QCommandLineParser>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QDebug>

using namespace SiberSystemsInc;

struct AppData
{
  QString rectsJsonFilename;
  QString outHtmlFilename;

  int htmlBorder = 0;
};

static const AppData parse_cmdline_opts(QCoreApplication & app)
{
  QCommandLineParser parser;
  parser.setApplicationDescription("SiberSystemsInc\nHTML Rectangles Table Generator");
  parser.addHelpOption();

  QCommandLineOption inOpt(QStringList() << "i" << "input", "<file> contains rectangles description (JSON format)", "file", "");
  parser.addOption(inOpt);
  QCommandLineOption outOpt(QStringList() << "o" << "output", "<file> HTML with rectangles", "file", "rects.html");
  parser.addOption(outOpt);

  QCommandLineOption tableBorderOpt(QStringList() << "b" << "table-border", "<val> HTML table border", "val", "0");
  parser.addOption(tableBorderOpt);

  parser.process(app);

  AppData d;

  d.rectsJsonFilename = parser.value(inOpt);
  if (d.rectsJsonFilename.isEmpty())
  {
    std::cerr << "Error! Rectangles file name not set, exit" << std::endl;
    std::exit(-1);
  }

  d.outHtmlFilename = parser.value(outOpt);
  if (d.outHtmlFilename.isEmpty())
  {
    std::cerr << "out html filename not set, using default" << std::endl;
    d.outHtmlFilename = outOpt.defaultValues().first();
  }

  bool ok = false;
  d.htmlBorder = parser.value(tableBorderOpt).toInt(&ok);
  if (!ok)
    std::cerr << "Warn! HTML border value parse failure" << std::endl;

  return d;
}

static int parse_rects_pos_value(const QJsonObject &json, const char * name, int idxRect)
{
  if (!(json.contains(name) && json[name].isDouble()))
    std::cerr << "parse rect #" << idxRect << ": " << name << " point pos not set, using default" << std::endl;
  return json[name].toInt();
}
static int parse_rects_color_value(const QJsonObject &json, const char * name, int idxRect)
{
  if (!(json.contains(name) && json[name].isString()))
    std::cerr << "parse rect #" << idxRect << ": " << name << " not set, using default" << std::endl;

  uint val = json[name].toString("888888").toUInt(0, 16);
  return val;
}

static const std::list<Rect> parse_rects_json(const QJsonObject &json)
{
  std::list<Rect> rects;

  if (json.contains("rects") && json["rects"].isArray())
  {
    QJsonArray rectsJsonArray = json["rects"].toArray();
    int idx = 0;
    for (const auto& rectJson : rectsJsonArray)
    {
      int left, top, right, bottom, color;

      const QJsonObject obj = rectJson.toObject();
      left = parse_rects_pos_value(obj, "left", idx);
      top = parse_rects_pos_value(obj, "top", idx);
      right = parse_rects_pos_value(obj, "right", idx);
      bottom = parse_rects_pos_value(obj, "bottom", idx);
      color = parse_rects_color_value(obj, "color", idx);

      rects.push_back(Rect(left,top,right,bottom,color));
      ++idx;
    }
  }

  return rects;
}
static const std::list<Rect> parse_rects_json_file(QString filename)
{
  QFile File(filename);

  if (!File.open(QIODevice::ReadOnly))
  {
    std::cerr << "Couldn't open "
              << filename.toLocal8Bit().constData()
              << " (" << File.errorString().toLocal8Bit().constData() << ")" << std::endl;
    return std::list<Rect>();
  }

  QByteArray jsonData = File.readAll();
  QJsonDocument jsonDoc(QJsonDocument::fromJson(jsonData));

  return parse_rects_json(jsonDoc.object());
}

int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);
  AppData d = parse_cmdline_opts(a);
  std::list<Rect> rects = parse_rects_json_file(d.rectsJsonFilename);
  if (rects.empty())
  {
    std::cerr << "Error! Rectangle List is empty, exit" << std::endl;
    return -1;
  }

  const char * htmlfilename = d.outHtmlFilename.toLocal8Bit().constData();
  std::cout << "Generate RectsTable HTML file \"" << htmlfilename << "\"...";
  RectTableGen::rects_to_html_file(htmlfilename, &rects, d.htmlBorder);
  std::cout << " done" << std::endl;

  return 0;
}
