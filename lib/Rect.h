#ifndef RECT_H
#define RECT_H

#include <list>

namespace SiberSystemsInc
{

enum Axis {axisX=0, axisY=1};
enum RectAxisPointType {pointFirst=0, pointLast=1};

class Rect
{
public:
  class Pos
  {
  public:
    int x = 0, y = 0;
  };

  Rect(int left, int top, int right, int bottom, int color = 0x888888, void * UserDataPtr = nullptr)
  {
    LeftTop.x = left;
    LeftTop.y = top;
    RightBottom.x = right;
    RightBottom.y = bottom;
    this->color = color;
    this->UserDataPtr = UserDataPtr;
  }

  struct less_than_left
  {
    inline bool operator() (const SiberSystemsInc::Rect* rect1, const SiberSystemsInc::Rect* rect2)
    {
      return (rect1->LeftTop.x < rect2->LeftTop.x);
    }
  };
  struct less_than_top
  {
    inline bool operator() (const SiberSystemsInc::Rect* rect1, const SiberSystemsInc::Rect* rect2)
    {
      return (rect1->LeftTop.y < rect2->LeftTop.y);
    }
  };
  struct less_than_right
  {
    inline bool operator() (const SiberSystemsInc::Rect* rect1, const SiberSystemsInc::Rect* rect2)
    {
      return (rect1->RightBottom.x < rect2->RightBottom.x);
    }
  };
  struct less_than_bottom
  {
    inline bool operator() (const SiberSystemsInc::Rect* rect1, const SiberSystemsInc::Rect* rect2)
    {
      return (rect1->RightBottom.y < rect2->RightBottom.y);
    }
  };

  Pos LeftTop, RightBottom;
  int color;
  void * UserDataPtr;
};

typedef std::list<Rect*> SortedRectList;

class SortedRectsIterator
{
public:
  struct IteratorData
  {
    IteratorData(SortedRectList * l, RectAxisPointType pt)
    {
      list = l;
      if (list != nullptr)
        iterator = list->begin();
      pointType = pt;
    }

    const bool isEnd() const {return iterator == list->end();}

    Rect* rect()
    {
      if (isEnd())
        return nullptr;
      Rect* r = (*iterator);
      return r;
    }

    int pos(Axis axis)
    {
      if (isEnd())
        return -1;
      auto r = rect();
      if (r == nullptr)
        return -1;
      switch(axis)
      {
      case axisX:
        switch(pointType)
        {
        case pointFirst:
          return r->LeftTop.x;
        case pointLast:
          return r->RightBottom.x;
        }
        break;
      case axisY:
        switch(pointType)
        {
        case pointFirst:
          return r->LeftTop.y;
        case pointLast:
          return r->RightBottom.y;
        }
        break;
      }
      return -1;
    }
    SortedRectList * list;
    SortedRectList::iterator iterator;
    RectAxisPointType pointType;
  };

  SortedRectsIterator(Axis axis)
  {
    this->axis = axis;
  }

  void resetIterator()
  {
    for (auto& d : iData)
      d.iterator = d.list->begin();
  }

  void append(SortedRectList * list, RectAxisPointType pointType)
  {
    iData.push_back(IteratorData(list, pointType));
  }

  IteratorData * current(){return iter_current_min();}

  const IteratorData next()
  {
    auto d = iter_current_min();
    if (d == nullptr)
      return IteratorData(0,pointFirst);
    auto d0 = *d;
    int curPos = d0.pos(axis);
    ++d->iterator;
    return d0;
  }

private:
  Axis axis;
  std::list<IteratorData> iData;

  IteratorData * iter_current_min()
  {
    IteratorData * min = nullptr;
    int minVal; // инциализируется на первом шаге
    for (auto& d : iData)
    {
      if (d.isEnd())
        continue;
      int val = d.pos(axis);
      if ((min == nullptr)||(val < minVal))
      {
        min = &d;
        minVal = val;
      }
    }
    return min;
  }
#ifdef QT_TESTLIB_LIB
  friend class TestRect;
#endif
};

} // SiberSistemsInc

#endif // RECT_H
