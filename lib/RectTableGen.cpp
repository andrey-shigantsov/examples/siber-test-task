#include "RectTableGen.h"

#include <cassert>

#include <algorithm>

#include <sstream>
#include <iomanip>
#include <fstream>

using namespace SiberSystemsInc;

std::string RectTableGen::rects_to_html(std::list<Rect>* rects, int border, int cellpadding, int cellspacing)
{
  RectTableGen gen;
  gen.load_rects(rects);
  gen.table_gen();
  return gen.table_to_html(border, cellpadding, cellspacing);
}

void RectTableGen::rects_to_html_file(const char * fileName, std::list<Rect>* rects, int border, int cellpadding, int cellspacing)
{
  std::ofstream ofs;
  ofs.open(fileName);
  ofs << "<!DOCTYPE html>" << std::endl
     << "<html>" << std::endl
     << "<head>" << std::endl
     << "<title>Rect Table - Test Task - Siber Systems Inc.</title>" << std::endl
     << "</head>" << std::endl
     << "<body>" << std::endl;
  ofs << rects_to_html(rects, border, cellpadding, cellspacing);
  ofs << "</body>" << std::endl
     << "</html>" << std::endl;
  ofs.close();
}

void RectTableGen::load_rects(std::list<Rect>* rects)
{
  this->rects = rects;

  for(auto& rect : *rects)
    rectsSorted[0][0].push_back(&rect);
  rectsSorted[0][1] = rectsSorted[0][0];
  for(int i = 1; i < 2; ++i)
    for(int j = 0; j < 2; ++j)
      rectsSorted[i][j] = rectsSorted[0][0];

  table = RectTable();
  for (auto& rect : *rects)
  {
    table.rects.push_back(RectTableRect(0,0,0,0,rect.color));
    rect.UserDataPtr = &table.rects.back();
  }

  sort_rects_by(axisX);
  sort_rects_by(axisY);
}

void RectTableGen::table_gen_proc_data(Axis axis, SortedRectsIterator::IteratorData * iD, int val0, int length, int * I)
{
  auto rect = iD->rect();
  auto rect_rect = static_cast<RectTableRect*>(rect == nullptr ? nullptr : rect->UserDataPtr);
  switch (axis)
  {
  case axisX:
    if (length)
    {
      table.columns.push_back(RectTableColumn(val0,length));
      ++(*I);
    }
    if (rect_rect == nullptr) break;
    switch(iD->pointType)
    {
    case pointFirst:
      rect_rect->colIdx = *I;
      break;
    case pointLast:
      rect_rect->colCount = *I - rect_rect->colIdx;
      break;
    }
    break;
  case axisY:
    if (length)
    {
      table.rows.push_back(RectTableRow(val0,length));
      ++(*I);
    }
    if (rect_rect == nullptr) break;
    switch(iD->pointType)
    {
    case pointFirst:
      rect_rect->rowIdx = *I;
      break;
    case pointLast:
      rect_rect->rowCount = *I - rect_rect->rowIdx;
      break;
    }
    break;
  }
}

void RectTableGen::table_gen_proc_axis(Axis axis)
{
  SortedRectsIterator iter(axis);
  for (int i = 0; i < 2; ++i)
    iter.append(&rectsSorted[axis][i], static_cast<RectAxisPointType>(i));

  int I = 0;
  auto iD = iter.next();
  int val0 = iD.pos(axis);
  table_gen_proc_data(axis,&iD,val0,0,&I);
  iD = iter.next();
  while (iD.list != nullptr)
  {
    if (iD.isEnd())
      break;

    int val = iD.pos(axis);
    int length = val - val0;
    table_gen_proc_data(axis,&iD,val0,length,&I);

    val0 = val;
    iD = iter.next();
  }
}

RectTable* RectTableGen::table_gen()
{
  table_gen_proc_axis(axisX);
  table_gen_proc_axis(axisY);

  return &table;
}

std::string RectTableGen::table_to_html(int border, int cellpadding, int cellspacing)
{
  std::stringstream ss;

  ss << "<table border=" << border
     << " cellpadding=" << cellpadding
     << " cellspacing=" << cellspacing
     << ">" << std::endl;

  ss << "\t<tr><td></td>";
  for(const auto& column : table.columns)
    ss << "<td width=" << column.width << ">";
  ss << "</tr>" << std::endl;

  for(auto& rect : table.rects)
    rect.UserDataPtr = nullptr;
  RectTableRect * curRect = nullptr;
  int iRow = 0;
  for(const auto& row : table.rows)
  {
    ss << "\t<tr><td height=" << row.height << "></td>" << std::endl;
    int iCol = 0, emptyColCount = 0;
#define PROC_EMPTY_COLS_END \
  do{ \
    if (emptyColCount > 0) \
    { \
      ss << "\t\t<td"; \
      if (emptyColCount > 1) \
        ss << " colspan=" << emptyColCount; \
      ss << "></td>" << std::endl; \
      emptyColCount = 0; \
    } \
  }while(0)
    for(const auto& column : table.columns)
    {
      if (curRect == nullptr)
      {
        bool isStartRect = false, isRectBody = false;
        for(auto& rect : table.rects)
        {
          if (rect.UserDataPtr != nullptr)
          {
            if (rect.UserDataPtr == (void*)0x2)
              continue;
            if ((iRow >= rect.rowIdx)&&(iRow <= rect.lastRowIdx())
                &&(iCol == rect.colIdx))
            {
              PROC_EMPTY_COLS_END;
              isRectBody = true;
              curRect = &rect;
              break;
            }
            continue;
          }
          if ((rect.rowIdx == iRow)&&(rect.colIdx == iCol))
          {
            PROC_EMPTY_COLS_END;
            isStartRect = true;
            rect.UserDataPtr = (void*)0x1;
            curRect = &rect;
            break;
          }
        }
        if (isStartRect)
        {
          ss << "\t\t<td";
          if (curRect->rowCount > 1)
            ss << " rowspan=" << curRect->rowCount;
          if (curRect->colCount > 1)
            ss << " colspan=" << curRect->colCount;
          ss << " bgcolor=\"#" << std::setfill('0') << std::setw(6) << std::hex << curRect->color << std::dec
             << "\"></td>" << std::endl;
        }
        else if (!isRectBody)
          ++emptyColCount;
      }
      if ((curRect != nullptr)&&(iCol == curRect->lastColIdx()))
      {
        if (iRow == curRect->lastRowIdx())
          curRect->UserDataPtr = (void*)0x2;
        curRect = nullptr;
      }
      ++iCol;
    }
    PROC_EMPTY_COLS_END;
    ss << "\t</tr>" << std::endl;
    ++iRow;
#undef PROC_EMPTY_COLS_END
  }

  ss << "</table>" << std::endl;

  return ss.str();
}

void RectTableGen::sort_rects_by(Axis axis)
{
  sort_rects_by(axis,pointFirst);
  sort_rects_by(axis,pointLast);
}

void RectTableGen::sort_rects_by(Axis axis, RectAxisPointType pointType)
{
  auto list = &rectsSorted[axis][pointType];
  switch (axis) // не понял как сделать указатель на оператор сравнения
  {
  case axisX:
    switch (pointType)
    {
    case pointFirst:
      list->sort(Rect::less_than_left());
      break;
    case pointLast:
      list->sort(Rect::less_than_right());
      break;
    }
    break;
  case axisY:
    switch (pointType)
    {
    case pointFirst:
      list->sort(Rect::less_than_top());
      break;
    case pointLast:
      list->sort(Rect::less_than_bottom());
      break;
    }
    break;
  }
}
