# HTML Rect Table - SiberSystemsInc Test Task

Приложение отрисовывает прмоугольники через HTML таблицу,
заданные через текстовый файл в формате JSON.
(Пример файла представлен в "rects-example.json")

## Сборка

Для сборки требуется:
- CMake 3.13 или выше
- Qt 5 (проверялось на 5.13)
- GCC подобнй компилятор (MSVC и др. не проверялись)
 
### Сборка через терминал Linux / Windows+MSYS/mingw/...

1. Подготовка
> cd <project-dir>
> mkdir ../build-html-rect-table
> cd ../build-html-rect-table
В Linux
> cmake -DCMAKE_BUILD_TYPE:STRING=Debug ../<project-dir>
В MSYS или mingw необходимо указать соответствующий генератор
> cmake -DCMAKE_BUILD_TYPE:STRING=Debug -G"MSYS Makefiles" ../<project-dir>


Проверялась только отладочная конфигурация сборки

2. Сборка приложения и всех тестов
> make

3. Выполнение всех тестов
> make test

### Сборка через QtCreator

Должна работать из коробки, достаточно просто открыть проект CMakeLists.txt

## Использование приложения html-rect-table

Для получения справки нужно выполнить команду
> html-rect-table -h
или
> html-rect-table --help

### Пример использования

Следующая команда сгенерирует файл "rects.html"
> html-rect-table -i"<project-dir>/rects-example.json"

Для указания выходного файла воспользуйтесь параметром -o
> html-rect-table -i"<project-dir>/rects-example.json" -o"<...>/<name>.html"

Также можно указать ширину границы таблицы (параметр -b)
> html-rect-table -i"<project-dir>/rects-example.json" -o"<...>/<name>.html" -b2

