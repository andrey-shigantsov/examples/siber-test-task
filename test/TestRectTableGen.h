#ifndef TESTRECTTABLEGEN_H
#define TESTRECTTABLEGEN_H

#include <QTest>
#include <RectTableGen.h>

namespace SiberSystemsInc
{

class TestRectTableGen : public QObject
{
  Q_OBJECT
private slots:
  void test_table_gen();
  void test_table_to_html();
  void test_sort_rects_by_axis();
};

} // SiberSistemsInc

#endif // TESTRECTTABLEGEN_H
