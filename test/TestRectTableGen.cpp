#include "TestRectTableGen.h"

#include <iostream>
#include <sstream>
#include <fstream>

#include <QDebug>

QTEST_MAIN(SiberSystemsInc::TestRectTableGen)

using namespace SiberSystemsInc;

void TestRectTableGen::test_sort_rects_by_axis()
{
  std::list<Rect> rects{Rect(0,0,6,5), Rect(1,6,3,10), Rect(4,7,7,8), Rect(7,4,8,6)};

  Q_ASSERT(rects.size() == 4);
  auto it = rects.begin();
  Rect* rectPtr[4];
  {
    for(int i = 0; i < 4; ++i)
      rectPtr[i] = &(*std::next(it,i));
  }

  SortedRectList rectsSorted[2][2];
  rectsSorted[axisX][pointFirst] = SortedRectList{rectPtr[0], rectPtr[1], rectPtr[2], rectPtr[3]};
  rectsSorted[axisX][pointLast] = SortedRectList{rectPtr[1], rectPtr[0], rectPtr[2], rectPtr[3]};
  rectsSorted[axisY][pointFirst] = SortedRectList{rectPtr[0], rectPtr[3], rectPtr[1], rectPtr[2]};
  rectsSorted[axisY][pointLast] = SortedRectList{rectPtr[0], rectPtr[3], rectPtr[2], rectPtr[1]};

  RectTableGen gen;
  gen.load_rects(&rects);

  Axis axis;
  RectAxisPointType pointType;

  axis = axisX; pointType = pointFirst;
  QVERIFY(gen.rectsSorted[axis][pointType] == rectsSorted[axis][pointType]);

  axis = axisX; pointType = pointLast;
  QVERIFY(gen.rectsSorted[axis][pointType] == rectsSorted[axis][pointType]);

  axis = axisY; pointType = pointFirst;
  QVERIFY(gen.rectsSorted[axis][pointType] == rectsSorted[axis][pointType]);

  axis = axisY; pointType = pointLast;
  QVERIFY(gen.rectsSorted[axis][pointType] == rectsSorted[axis][pointType]);

}

void TestRectTableGen::test_table_gen()
{
  std::list<Rect> rects{Rect(0,0,6,5), Rect(1,6,3,10), Rect(4,7,7,8), Rect(7,4,8,6)};

  RectTable table0;
  //
  table0.columns.push_back(RectTableColumn(0,1));
  table0.columns.push_back(RectTableColumn(1,2));
  table0.columns.push_back(RectTableColumn(3,1));
  table0.columns.push_back(RectTableColumn(4,2));
  table0.columns.push_back(RectTableColumn(6,1));
  table0.columns.push_back(RectTableColumn(7,1));
  //
  table0.rows.push_back(RectTableRow(0,4));
  table0.rows.push_back(RectTableRow(4,1));
  table0.rows.push_back(RectTableRow(5,1));
  table0.rows.push_back(RectTableRow(6,1));
  table0.rows.push_back(RectTableRow(7,1));
  table0.rows.push_back(RectTableRow(8,2));
  //
  table0.rects.push_back(RectTableRect(0,2,0,4));
  table0.rects.push_back(RectTableRect(3,3,1,1));
  table0.rects.push_back(RectTableRect(4,1,3,1));
  table0.rects.push_back(RectTableRect(1,2,5,1));

  RectTableGen gen;
  gen.load_rects(&rects);
  auto table = gen.table_gen();

  QVERIFY(*table == table0);
}

static const char * test_table_to_html_data_htmlStr0()
{
  return  "<table border=2 cellpadding=0 cellspacing=1> "
          "<tr><td></td><td width=10><td width=20><td width=10><td width=20><td width=10><td width=10></tr> "
          "<tr><td height=40></td> <td rowspan=2 colspan=4 bgcolor=\"#ff0000\"></td> <td colspan=2></td> </tr> "
          "<tr><td height=10></td> <td></td> <td rowspan=2 bgcolor=\"#00ff00\"></td> </tr> "
          "<tr><td height=10></td> <td colspan=4></td> </tr> "
          "<tr><td height=10></td> <td></td> <td rowspan=3 bgcolor=\"#0000ff\"></td> <td colspan=4></td> </tr> "
          "<tr><td height=10></td> <td colspan=2></td> <td colspan=2 bgcolor=\"#f000f0\"></td> <td></td> </tr> "
          "<tr><td height=20></td> <td colspan=5></td> </tr> "
          "</table>";
}
void TestRectTableGen::test_table_to_html()
{
  std::list<Rect> rects{Rect(0,0,60,50,0xFF0000), Rect(10,60,30,100,0x0000FF), Rect(40,70,70,80,0xF000F0), Rect(70,40,80,60,0x00FF00)};
  auto htmlStr0 = QString(test_table_to_html_data_htmlStr0());

  std::string htmlStdString = RectTableGen::rects_to_html(&rects,2,0,1);
  std::ofstream ofs;
  ofs.open("testTable.html");
  ofs << htmlStdString;
  ofs.close();

  auto htmlStr = QString::fromStdString(htmlStdString).simplified();

  QVERIFY(htmlStr == htmlStr0);
}
