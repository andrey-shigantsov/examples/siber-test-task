#ifndef TESTRECT_H
#define TESTRECT_H

#include <QTest>
#include <Rect.h>

namespace SiberSystemsInc
{

class TestRect : public QObject
{
  Q_OBJECT
private slots:
  void test_SortedRectsIterator_next();
};

} // SiberSistemsInc

#endif // TESTRECT_H
