#include "TestRect.h"

#include <cassert>

#include <QDebug>

QTEST_MAIN(SiberSystemsInc::TestRect)

using namespace SiberSystemsInc;

void TestRect::test_SortedRectsIterator_next()
{
  std::list<Rect> rects{Rect(0,0,6,5, 0x888888, (void*)0), Rect(1,6,3,10, 0x888888, (void*)1), Rect(4,7,7,8, 0x888888, (void*)2), Rect(7,4,8,6, 0x888888, (void*)3)};

  Q_ASSERT(rects.size() == 4);
  auto it = rects.begin();
  Rect* rectPtr[4];
  {
    for(int i = 0; i < 4; ++i)
      rectPtr[i] = &(*std::next(it,i));
  }

  SortedRectList rectsSorted[2][2];
  rectsSorted[axisX][pointFirst] = SortedRectList{rectPtr[0], rectPtr[1], rectPtr[2], rectPtr[3]};
  rectsSorted[axisX][pointLast] = SortedRectList{rectPtr[1], rectPtr[0], rectPtr[2], rectPtr[3]};
  rectsSorted[axisY][pointFirst] = SortedRectList{rectPtr[0], rectPtr[3], rectPtr[1], rectPtr[2]};
  rectsSorted[axisY][pointLast] = SortedRectList{rectPtr[0], rectPtr[3], rectPtr[2], rectPtr[1]};

  std::list<int> valsSorted[2], valsSorted2[2],
  valsSorted0[2] =
  {
    {0, 1, 3, 4, 6, 7, 7, 8},
    {0, 4, 5, 6, 6, 7, 8, 10}
  };

  {
    Axis axis = axisX;
    SortedRectsIterator iter(axis);
    for (int j = 0; j < 2; ++j)
      iter.append(&rectsSorted[axis][j], static_cast<RectAxisPointType>(j));

    auto iD = iter.iter_current_min();
    while ((iD != nullptr)&&(!iD->isEnd()))
    {
      valsSorted[axis].push_back(iD->pos(axis));
      ++iD->iterator;
      iD = iter.iter_current_min();
    }
    QVERIFY(valsSorted[axis] == valsSorted0[axis]);

    iter.resetIterator();

    auto iDu = iter.next();
    while ((iDu.list != nullptr)&&(!iDu.isEnd()))
    {
      valsSorted2[axis].push_back(iDu.pos(axis));
      iDu = iter.next();
    }
    QVERIFY(valsSorted2[axis] == valsSorted0[axis]);
  }
  {
//    Axis axis = axisY;
//    SortedRectsIterator iter(axis);
//    for (int j = 0; j < 2; ++j)
//      iter.append(&rectsSorted[axis][j], static_cast<RectAxisPointType>(j));
   }
}
